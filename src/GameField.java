import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.util.Random;
import java.awt.Graphics;


/**
 * Created by dev on 2/4/14.
 */
public class GameField extends JPanel
{
    private Snake snake;
    private Rabbit rabbit;
    public GameField()
    {
        this.createSnake();
        this.createRabbit();

    }
    public void createSnake()
    {
        snake = new Snake();

        int x_AppearSnake = RandomX();
        int y_AppearSnake = RandomY();

        snake.setText("S");
        snake.setSize(45,45 );
        snake.setVisible(true);
        snake.setLocation(x_AppearSnake,y_AppearSnake);
        snake.setEnabled(false);

        this.add(snake);
    }
    public void createRabbit()
    {
        rabbit = new Rabbit();

        int x_AppearRabbit = RandomX();
        int y_AppearRabbit = RandomY();

        rabbit.setSize(45,45);
        rabbit.setLocation(x_AppearRabbit, y_AppearRabbit);

        rabbit.setText("R");
        rabbit.setEnabled(false);

        this.add(rabbit);
        rabbit.setVisible(true);
    }
    public int RandomX()
    {
        Random random = new Random();
        return random.nextInt(1000-51);
    }
    public int RandomY()
    {
        Random random = new Random();
        return random.nextInt(700-74);
    }

    public Rabbit getRabbit()
    {
        return rabbit;
    }

    public Snake getSnake()
    {
        return snake;
    }

    public boolean isCrashed(Snake snake,Rabbit rabbit)
    {

        int sp1x,sp1y,sp2x,sp2y,sp3x,sp3y,sp4x,sp4y;
        int rp1x,rp1y,rp2x,rp2y,rp3x,rp3y,rp4x,rp4y;

        sp1x = snake.getX();
        sp2x = snake.getX()+45;
        sp3x = snake.getX();
        sp4x = snake.getX()+45;

        sp1y = snake.getY();
        sp2y = snake.getY();
        sp3y = snake.getY()+45;
        sp4y = snake.getY()+45;

        rp1x = rabbit.getX();
        rp2x = rabbit.getX()+45;
        rp3x = rabbit.getX();
        rp4x = rabbit.getX()+45;


        rp1y = rabbit.getY();
        rp2y = rabbit.getY();
        rp3y = rabbit.getY()+45;
        rp4y = rabbit.getY()+45;

        if((sp1x <= rp4x && sp1x >= rp3x) && (sp1y <= rp4y && sp1y >= rp2y))
            return true;

        if(sp2x >= rp3x && sp2x <= rp4x  && sp2y <= rp3y && sp2y >= rp1y)
            return true;

        if(sp3x <= rp2x && sp3x >= rp1x  && sp3y >= rp2y && sp3y <= rp4y)
            return true;

        if(sp4x >= rp1x && sp4x <= rp2x  && sp4y >= rp1y && sp4y <= rp3y)
            return true;

        return false;
    }

    public boolean isBorder(Snake snake, KeyEvent keyEvent)
    {
        int sp2x = snake.getX()+60;
        int sp4y = snake.getY()+45;
        int sp1x = snake.getX();



        if(sp2x >= 985 && keyEvent.getKeyCode() == KeyEvent.VK_RIGHT)
            return true;

        if(sp4y > 665 && keyEvent.getKeyCode() == KeyEvent.VK_DOWN)
            return true;

        if(sp4y <=  35&& keyEvent.getKeyCode() == KeyEvent.VK_UP)
            return true;

        if(sp1x < 0 && keyEvent.getKeyCode() == KeyEvent.VK_LEFT)
            return true;

        return false;
    }





}