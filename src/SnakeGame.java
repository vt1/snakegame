import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;


/**
 * Created by dev on 2/4/14.
 */
public class SnakeGame extends JFrame implements KeyListener, ActionListener
{
    private GameField gameField;
    private boolean isCrashed;
    private boolean isBorder;
    private int rabbitCount;

    public SnakeGame()
    {
        this.rabbitCount = 0;
        gameField = new GameField();
        gameField.setVisible(true);
        gameField.setBackground(Color.WHITE);
        gameField.setLayout(null);

        this.add(gameField);
        this.setAutoRequestFocus(true);

        this.addKeyListener(this);
        this.setResizable(false);
    }



    @Override
    public void actionPerformed(ActionEvent e) {

    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e)
    {
        if(e.getKeyCode() == KeyEvent.VK_UP)
        {
            Snake snake = gameField.getSnake();
            Rabbit rabbit = gameField.getRabbit();

            int snakeX = snake.getX();
            int snakeY = snake.getY();

            snake.setLocation(snakeX,snakeY-45);

            isCrashed = gameField.isCrashed(snake,rabbit);
            if(isCrashed)
            {
                recreateRabbit(rabbit);
            }

            isBorder = gameField.isBorder(snake, e);
            if(isBorder)
            {
                snake.setLocation(snake.getX(), 616);
                isCrashed = gameField.isCrashed(snake,rabbit);
                if(isCrashed)
                {
                    recreateRabbit(rabbit);
                }
            }
        }

        if(e.getKeyCode() == KeyEvent.VK_DOWN)
        {
            Snake snake = gameField.getSnake();
            Rabbit rabbit = gameField.getRabbit();

            int snakeX = snake.getX();
            int snakeY = snake.getY();

            snake.setLocation(snakeX,snakeY+45);

            isCrashed = gameField.isCrashed(snake,rabbit);
            if(isCrashed)
            {
                recreateRabbit(rabbit);
            }
            isBorder = gameField.isBorder(snake,e);
            if(isBorder)
            {
                snake.setLocation(snake.getX(),0);
                isCrashed = gameField.isCrashed(snake,rabbit);
                if(isCrashed)
                {
                    recreateRabbit(rabbit);
                }
            }


        }
        if(e.getKeyCode() == KeyEvent.VK_RIGHT)
        {
            Snake snake = gameField.getSnake();
            Rabbit rabbit = gameField.getRabbit();

            int snakeX = snake.getX();
            int snakeY = snake.getY();

            snake.setLocation(snakeX+45,snakeY);

            isCrashed = gameField.isCrashed(snake,rabbit);
            if(isCrashed)
            {
                recreateRabbit(rabbit);
            }
            isBorder = gameField.isBorder(snake,e);
            if(isBorder)
            {
                snake.setLocation(0,snake.getY());
                isCrashed = gameField.isCrashed(snake,rabbit);
                if(isCrashed)
                {
                    recreateRabbit(rabbit);
                }
            }

        }
        if(e.getKeyCode() == KeyEvent.VK_LEFT)
        {
            Snake snake = gameField.getSnake();
            Rabbit rabbit = gameField.getRabbit();

            int snakeX = snake.getX();
            int snakeY = snake.getY();

            snake.setLocation(snakeX-45,snakeY);
            isCrashed = gameField.isCrashed(snake,rabbit);
            if(isCrashed)
            {
                recreateRabbit(rabbit);
            }
            isBorder = gameField.isBorder(snake,e);
            if(isBorder)
            {
                snake.setLocation(939,snake.getY());
                isCrashed = gameField.isCrashed(snake,rabbit);
                if(isCrashed)
                {
                    recreateRabbit(rabbit);
                }
            }
        }
    }


    @Override
    public void keyReleased(KeyEvent e) {

    }

    public void recreateRabbit(Rabbit rabbit)
    {
        gameField.remove(rabbit);
        gameField.createRabbit();
        gameField.repaint();
        this.rabbitCount++;
        this.setTitle("Rabbits: " + Integer.toString(this.rabbitCount));
    }
}

